<?php
/* * Template Name: Research Page 
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 *
 * @package _tk
 */

get_header(); ?>


<div class="main-content pt70">
<?php while ( have_posts() ) : the_post(); ?>

<?php 
			$thumbnail = '';
			if (function_exists('has_post_thumbnail')) {
			    if ( has_post_thumbnail() ) {
					 $thumbnail =  wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			    } else { 
			    	 $thumbnail = get_bloginfo( 'stylesheet_directory' )  . '/images/home-header.jpg';}
			} 
		?>
<div id="page-header" >
<div id="bg" style="background: url('<?php echo $thumbnail; ?>') no-repeat;">
</div>
<div id="cover">
<p>
<h1><?php the_title();?></h1>
</p>
</div>
</div>
<div class="skewed-bg black">
	<div class="container" >
		<?php if( have_rows('research_box1') ):  while ( have_rows('research_box1') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><?php the_sub_field('title');?></h4>
	     	<div class="text">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>
		<?php if( have_rows('research_box2') ):  while ( have_rows('research_box2') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><?php the_sub_field('title');?></h4>
	     	<div class="text">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>
		<?php if( have_rows('research_box3') ):  while ( have_rows('research_box3') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><?php the_sub_field('title');?></h4>
	     	<div class="text">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>
		<?php if( have_rows('research_box4') ):  while ( have_rows('research_box4') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><?php the_sub_field('title');?></h4>
	     	<div class="text">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>
	</div>
</div>
<div id="research-blackcap">
	
</div>

<?php if( have_rows('chart_section') ):  while ( have_rows('chart_section') ) : the_row(); if( get_row_layout() == 'chart_text' ):?>
<div id="research-blackcap-lower">
	<div class="container" >
		<div class="cnt-wrp">
			<h3 class="yellow"><?php the_sub_field('title');?></h3>
			<?php the_sub_field('text');?>
		<img class="research-chart" src="/wp-content/uploads/2016/05/sprint8-arrowChart.png" >
		</div>
	</div>
</div>
<?php endif; endwhile; endif;	?>

<?php if( have_rows('white_solid_section') ):  while ( have_rows('white_solid_section') ) : the_row(); if( get_row_layout() == 'add_text' ):?>
<div class="skewed-bg white z50">
	<div class="content" id="natural" >
		<div class="container">
		<h3 class="yellow"><?php the_sub_field('title');?></h3>
		<?php the_sub_field('text');?>
		</div>
	</div>
</div>
<?php endif; endwhile; endif;	?>

<div id="about-phil">
	<div class="container">
	<div class="cnt-wrp col-sm-12 col-md-8">
		<?php the_field('about_phill_section') ;?>
	<div class="phil-link-wrp col-sm-12 col-md-5">
		<a href="http://www.amazon.com/Sprint-Cardio-Protocol-Phil-Campbell/dp/0692650024/ref=sr_1_1?ie=UTF8&qid=1464966291&sr=8-1&keywords=sprint+8+cardio+protocol" target="_blank" class="gray-btn">Buy Ready, Set, Go!</a>
		<span>(US customers only)</span>
	</div> 
	<div class="phil-link-wrp col-sm-12 col-md-5">
		<a href="#" class="gray-btn" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#philModal">Get In Touch With Phil</a>
	</div>
	

<!-- Modal -->
<div class="modal fade" id="philModal" tabindex="-1" role="dialog" aria-labelledby="philModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="container">
        <?php echo do_shortcode( '[contact-form-7 id="540" title="Phil Form"]' ); ?>
        </div>
      </div>
      
    </div>
  </div>
</div>
	</div>
</div>

</div>

<div class="skewed-bg black">
	<div class="content" id="resource" >
		<h3><?php echo get_theme_mod('bottom-posts-title');?></h3>
		
    <div class="resources-wrp">
      <?php 
      $setcounter = 0;
      $sticky = get_option( 'sticky_posts' );
      rsort( $sticky );
      $sticky = array_slice( $sticky, 0, 4 );
      $query = new WP_Query( array( 'post__in' => $sticky, 'ignore_sticky_posts' => 1 ) );
      ?>
      <?php while ($query->have_posts()) : $query->the_post(); $setcounter++; ?>
      <?php 
			$postthumb = '';
			if (function_exists('has_post_thumbnail')) {
			    if ( has_post_thumbnail() ) {
					 $postthumb =  wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			    } else { 
			    	 $postthumb = get_bloginfo( 'stylesheet_directory' )  . '/images/home-header.jpg';}
			} 
		?>
      
        <div id="resource-bx-<?php echo $setcounter ?>" >
        	<div class="padding-wrp">
	        	<div class="bx-wrp">
	        		<a href="<?php if ( get_field( 'link_url' ) ): ?><?php the_field('link_url');?><?php else: ?><?php the_permalink();?><?php endif; ?>" target="<?php if ( get_field( 'link_url' ) ): ?>_blank><?php else: ?>_self<?php endif; ?>" class="resource-bx-inner" style="background:url('<?php echo($postthumb); ?>')">            	
	                <h3><?php the_title();?></h3>
	           	    	<?php if ( get_field( 'link_url' ) ): ?>
						<div class="resource_link"><?php the_field('link_title');?></div>
						<?php else: ?>
						<div class="resource_link">Learn More</div>
						<?php endif; ?>
		            </a>
	            </div>
        	</div>
        </div>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    </div>

	</div>
</div>
<div id="resource-blackcap">
		<div class="cap-inner-bg "></div>

</div>

<?php endwhile; // end of the loop. ?>
</div><!-- close .main-content -->



		
<?php get_footer(); ?>
