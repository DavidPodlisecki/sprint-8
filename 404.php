<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package _tk
 */

get_header(); ?>




<div class="main-content pt70">


<div id="page-header" >
<div id="bg" style="background: url('<?php echo $thumbnail; ?>') no-repeat;">
</div>
<div id="cover">
<p>
<h1 class="post-title"> <?php echo  get_theme_mod( '404page_title'	); ?></h1>
</p>
</div>
</div>

<div class="skewed-bg white">
	<div class="container" id="page-cnt">
		
		<!-- <p><?php _e( 'Nothing could be found at this location. Maybe try a search?', '_tk' ); ?></p> -->
		<p><?php echo get_theme_mod('404textarea');?></p>

			<?php get_search_form(); ?>

	</div>
</div>


	</div><!-- close .container -->
</div><!-- close .main-content -->



<div class="skewed-bg black">
	<div class="content" id="resource">
		<h3>Resources</h3>
		
    <div class="resources-wrp">
      <?php 
      $setcounter = 0;
      $sticky = get_option( 'sticky_posts' );
      rsort( $sticky );
      $sticky = array_slice( $sticky, 0, 4 );
      $query = new WP_Query( array( 'post__in' => $sticky, 'ignore_sticky_posts' => 1 ) );
      ?>
      <?php while ($query->have_posts()) : $query->the_post(); $setcounter++; ?>
      <?php 
			$postthumb = '';
			if (function_exists('has_post_thumbnail')) {
			    if ( has_post_thumbnail() ) {
					 $postthumb =  wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			    } else { 
			    	 $postthumb = get_bloginfo( 'stylesheet_directory' )  . '/images/home-header.jpg';}
			} 
		?>
      
        <div id="resource-bx-<?php echo $setcounter ?>" >
        	<div class="padding-wrp">
	        	<div class="bx-wrp">
	        		<a href="<?php if ( get_field( 'link_url' ) ): ?><?php the_field('link_url');?><?php else: ?><?php the_permalink();?><?php endif; ?>" target="<?php if ( get_field( 'link_url' ) ): ?>_blank><?php else: ?>_self<?php endif; ?>" class="resource-bx-inner" style="background:url('<?php echo($postthumb); ?>')">            	
	                	<h3><?php the_title();?></h3>
	                	

	                	<?php if ( get_field( 'link_url' ) ): ?>
						<div class="resource_link"><?php the_field('link_title');?></div>
						<?php else: ?>
						<div class="resource_link">Learn More</div>
						<?php endif; ?>
		            </a>
	            </div>
        	</div>
        </div>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    </div>

	</div>
</div>
<div id="resource-blackcap">
		<div class="cap-inner-bg "></div>

</div>
<?php get_footer(); ?>