<?php
/* * Template Name: FAQ's Page 
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 *
 * @package _tk
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="main-content pt70">

<?php 
			$thumbnail = '';
			if (function_exists('has_post_thumbnail')) {
			    if ( has_post_thumbnail() ) {
					 $thumbnail =  wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			    } else { 
			    	 $thumbnail = get_bloginfo( 'stylesheet_directory' )  . '/images/home-header.jpg';}
			} 
		?>
<div id="page-header" >
<div id="bg" style="background: url('<?php echo $thumbnail; ?>') no-repeat;">
</div>
<div id="cover">
<p>
<h1><?php the_title();?></h1>
</p>
</div>
</div>

<div class="skewed-bg white">
	
</div>
<div class="container" id="faq">


<div class="panel-group" id="accordion">
  

  <?php if( have_rows('add_question') ): $questioncounter = 0; ?>
  <?php while ( have_rows('add_question') ) : the_row(); $questioncounter++;?>
                          
  <div class="panel panel-default">
    <div class="chat-bubble">
          <?php echo ($questioncounter);?>
          <div class="chat-bubble-arrow-border"></div>
          <div class="chat-bubble-arrow"></div>
        </div>
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo ($questioncounter);?>">
          <?php if ($questioncounter == 1) :?><span class="glyphicon glyphicon-minus"></span> <?php else: ?> <span class="glyphicon glyphicon-plus"></span> <?php endif ?>
          <?php the_sub_field('question'); ?>
        </a>
      </h4>
    </div>
    <div id="collapse-<?php echo ($questioncounter);?>" class="panel-collapse collapse  <?php if ($questioncounter == 1) {echo "in";} ?>">
      <div class="panel-body">
         <?php the_sub_field('answer'); ?>
      </div>
    </div>
  </div>
  <?php endwhile; else :  // no rows found ?>
  <?php endif;?>    
<ul>

</div>


<div id="faq-contact">
		<h4><?php the_content();?></h4>
		<a href="/contact" class="gray-btn">ASK A QUESTION</a>
	</div>
</div>

	</div>
	</div><!-- close .container -->
</div><!-- close .main-content -->
<script type="text/javascript">
jQuery( document ).ready( function( $ ) {

    //var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
    //$active.find('a').prepend('<i class="glyphicon glyphicon-minus"></i>');
    //$('#accordion .panel-heading').not($active).find('a').prepend('<i class="glyphicon glyphicon-plus"></i>');
    //$('#accordion').on('show.bs.collapse', function (e) {
    //    $('#accordion .panel-heading.active').removeClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
    //    $(e.target).prev().addClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
    //})
    $('.collapse').on('shown.bs.collapse', function(){
    $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hidden.bs.collapse', function(){
    $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });
});

</script>
			
			<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
