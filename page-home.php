<?php
/* * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 *
 * @package _tk
 */

get_header(); ?>
<div class="main-content pt70">
<?php while ( have_posts() ) : the_post(); ?>
	<div class="container-fluid">
		<?php if( have_rows('homepage_content_sections') ):  while ( have_rows('homepage_content_sections') ) : the_row(); if( get_row_layout() == 'slider' ):?>
		<div class="row">
			<div class="my-slider">
				<ul>
					<?php if( have_rows('slide') ): ?>
		        	<?php while ( have_rows('slide') ) : the_row();?>
		        	<li style="background-image:url('<?php the_sub_field('slide_image'); ?>')"><h2><?php the_sub_field('slide_title'); ?></h2></li>
		        	<?php endwhile; else :  // no rows found ?>
		        	<?php endif;?>
				</ul>
			</div>
		</div><!-- close .row -->
		<?php endif; endwhile; endif;	?>
	</div>
	<?php if( have_rows('homepage_content_sections') ):  while ( have_rows('homepage_content_sections') ) : the_row(); if( get_row_layout() == 'yellow_section' ):?>			
	<div class="skewed-bg yellow">
		<div class="content">
			<div id="hm-bg-cir"></div>
			<div id="hm-cnt-wrp" class="col-sm-12 col-md-9 col-lg-6">
				
		     	<div class="text">
		     		<p class="col-xs-9 col-sm-6 col-md-6 col-lg-6"><img class="sp-logo" src="/wp-content/uploads/2016/05/sprint8-LGwhite.png">
		     			<?php the_sub_field('text'); ?></p>
					<img class="chart wow fadeInDown" src="/wp-content/uploads/2016/05/sprint8-arrowChart.png"  data-wow-duration=".9s" data-wow-delay=".9s">
					
				</div>
				
			</div>
		</div>
	</div>
	<?php endif; endwhile; endif;	?>
	<?php if( have_rows('homepage_content_sections') ):  while ( have_rows('homepage_content_sections') ) : the_row(); if( get_row_layout() == 'mid_image_section' ):?>
	<div class="img-block" style="background-image:url('<?php the_sub_field('background_image'); ?>');">
		<div class="col-sm-9 col-md-5 col-lg-4 no-float">
			<h3><?php the_sub_field('title'); ?></h3>
			<?php the_sub_field('text'); ?>
			<a href="<?php the_sub_field('link_url'); ?>" class="gray-btn">See The Workout</a>
		</div>
	</div>
	<?php endif; endwhile; endif;	?>
	<?php if( have_rows('homepage_content_sections') ):  while ( have_rows('homepage_content_sections') ) : the_row(); if( get_row_layout() == 'bottom_text' ):?>
	<div class="skewed-bg black">
		<div class="content padding" >
			<div id="home-lower-bg"></div>
			<h3><?php the_sub_field('title');?></h3>
	     	<div class="text">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
	</div>
	<?php endif; endwhile; endif;	?>
<div id="home-lower">
	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	<img src="http://www.sprint8.com.php56-3.ord1-1.websitetestlink.com/wp-content/themes/Sprint8/includes/images/SVG-chart.svg" class="home-chart">
	<a href="/results" class="gray-btn">Get The Results</a>
	</div>
</div>

	</div><!-- close .container -->

<?php endwhile; // end of the loop. ?>
</div><!-- close .main-content -->

<script type="text/javascript">
jQuery( document ).ready( function( $ ) {

 $('.my-slider').unslider({
keys: false, 
arrows:true	,
 nav: false,
 autoplay:true,
 speed:600,
 delay:5000
});

} );


  var wow = new WOW();
  wow.init();

</script>
<?php get_footer(); ?>
