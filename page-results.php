<?php
/* * Template Name: Results Page 
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 *
 * @package _tk
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="main-content pt70">

<?php 
			$thumbnail = '';
			if (function_exists('has_post_thumbnail')) {
			    if ( has_post_thumbnail() ) {
					 $thumbnail =  wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			    } else { 
			    	 $thumbnail = get_bloginfo( 'stylesheet_directory' )  . '/images/home-header.jpg';}
			} 
		?>
<div id="page-header" >
<div id="bg" style="background: url('<?php echo $thumbnail; ?>') no-repeat;">
</div>
<div id="cover">
<p>
<h1><?php the_title();?></h1>
</p>
</div>
</div>

<div class="skewed-bg white">
	<div class="container" id="benifits">
		<?php the_content();?>
		<?php if( have_rows('research_box1') ):  while ( have_rows('research_box1') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><div class="chat-bubble">
		  1
		  <div class="chat-bubble-arrow-border"></div>
		  <div class="chat-bubble-arrow"></div>
		</div><?php the_sub_field('title');?></h4>
	     	<div class="text dk-txt">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>
		<?php if( have_rows('research_box2') ):  while ( have_rows('research_box2') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><div class="chat-bubble">
		  2
		  <div class="chat-bubble-arrow-border"></div>
		  <div class="chat-bubble-arrow"></div>
		</div><?php the_sub_field('title');?></h4>
	     	<div class="text dk-txt">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>
		<?php if( have_rows('research_box3') ):  while ( have_rows('research_box3') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><div class="chat-bubble">
		  3
		  <div class="chat-bubble-arrow-border"></div>
		  <div class="chat-bubble-arrow"></div>
		</div><?php the_sub_field('title');?></h4>
	     	<div class="text dk-txt">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>
		<?php if( have_rows('research_box4') ):  while ( have_rows('research_box4') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><div class="chat-bubble">
		  4
		  <div class="chat-bubble-arrow-border"></div>
		  <div class="chat-bubble-arrow"></div>
		</div><?php the_sub_field('title');?></h4>
	     	<div class="text dk-txt">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>
	</div>
</div>

<div class="skewed-bg black">
	<div class="container">
		<h3><?php the_field('mid-title');?></h3>
		
		<?php if( have_rows('black_box1') ):  while ( have_rows('black_box1') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><?php the_sub_field('title');?></h4>
	     	<div class="text ">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>

		<?php if( have_rows('black_box2') ):  while ( have_rows('black_box2') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><?php the_sub_field('title');?></h4>
	     	<div class="text ">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>

		<?php if( have_rows('black_box3') ):  while ( have_rows('black_box3') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><?php the_sub_field('title');?></h4>
	     	<div class="text ">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>

		<?php if( have_rows('black_box4') ):  while ( have_rows('black_box4') ) : the_row(); if( get_row_layout() == 'box' ):?>
		<div class="col-sm-12 col-md-6 row-bx">
			<h4><?php the_sub_field('title');?></h4>
	     	<div class="text ">
	     		<?php the_sub_field('text');?>
			</div>
		</div>
		<?php endif; endwhile; endif;	?>
	</div>
</div>

<div class="skewed-bg white">
	
	<div class="container" id="successes">
		<h3>Successes</h3>
		<?php // if( have_rows('homepage_content_sections') ):  while ( have_rows('homepage_content_sections') ) : the_row(); if( get_row_layout() == 'slider' ):?>
		<div class="successes-slider">
			<ul>
				<?php if( have_rows('successes') ): ?>
		        	<?php while ( have_rows('successes') ) : the_row();?>
		        	<li>
		        		<div class="infoImg col-xs-12 col-sm-12 col-md-6" style="background: url('<?php the_sub_field('slide_image'); ?>') no-repeat;">
						</div>
		        		<div class="infoQuote col-xs-12 col-sm-12 col-md-6" >
						<h5><?php the_sub_field('name'); ?></h5>
						<h6><?php the_sub_field('sub_title'); ?></h6>
						<p><?php the_sub_field('quote'); ?></p>
					</div>
		        	</li>
		        	<?php endwhile; else :  // no rows found ?>
		        	<?php endif;?>
			</ul>
		</div>
        <?php // endif; endwhile; endif;	?>


		<h3 class="red">PRESS AND PRAISE</h3>
		<!-- <img src="/wp-content/uploads/2016/05/Press-and-Praise.png" class="aligncenter"/> -->
		<div class="row press-praise">
			<div class="col-md-4">
				<a href="/wp-content/uploads/2016/06/o_magazine.pdf" target="_blank">
					<img src="/wp-content/uploads/2016/06/oprah-magazine.png" class="aligncenter">
				</a>
			</div>
			<div class="col-md-4">
				<a href="kings-daughter-medical-center/">
					<img src="/wp-content/uploads/2016/06/kings-daughters.png" class="aligncenter">
				</a>
			</div>
			<div class="col-md-4">
				<a href="/wp-content/uploads/2016/06/outside_magazine.pdf" target="_blank">
					<img src="/wp-content/uploads/2016/06/outside-magazine.png" class="aligncenter">
				</a>
			</div>
		</div>
		<div class="row press-praise">
			<div class="col-md-6">
				<a href="/wp-content/uploads/2016/06/la_times.pdf" target="_blank">
					<img src="/wp-content/uploads/2016/06/la-times.png" class="aligncenter">
				</a>
			</div>
			<div class="col-md-6">
				<a href="/wp-content/uploads/2016/06/SNEWS.pdf" target="_blank">
					<img src="/wp-content/uploads/2016/06/snews.png" class="aligncenter">
				</a>
			</div>
		</div>
	</div>
</div>
	</div><!-- close .container -->
</div><!-- close .main-content -->

		

			<?php endwhile; // end of the loop. ?>
<script type="text/javascript">
jQuery( document ).ready( function( $ ) {

 $('.successes-slider').unslider({
keys: false, 
arrows:true	,
 nav: false,
 autoplay:false,
 speed:600,

});

} );


  var wow = new WOW();
  wow.init();

</script>
<?php get_footer(); ?>
