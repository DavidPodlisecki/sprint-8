<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package _tk
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

	<script src="https://use.typekit.net/xpc3lxe.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<!-- bxSlider Javascript file -->
		<link href="<?php echo (get_template_directory_uri() . '/includes/css');?>/animate.css" rel="stylesheet" />
 
	<script src="<?php echo (get_template_directory_uri() . '/includes/js');?>/unslider-min.js"></script>
	<script src="<?php echo (get_template_directory_uri() . '/includes/js');?>/wow.min.js"></script>

	<!-- bxSlider CSS file -->
	<link href="<?php echo (get_template_directory_uri() . '/includes/css');?>/unslider.css" rel="stylesheet" />
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-79277971-1', 'auto');
 ga('send', 'pageview');

</script>
</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' ); ?>

<header id="masthead" class="site-header" role="banner">



<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container-fluid navbar-default navbar-fixed-top">
		<div class="row">
			<nav class="site-navigation ">
			<div class="site-branding col-sm-2 col-md-2">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="">
				</a>	
			</div>
			<div class="site-navigation-inner col-sm-9 col-md-9">
				
				<div class="navbar navbar-default alignright">
					<div class="navbar-header">
						<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only"><?php _e('Toggle navigation','_tk') ?> </span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
	
					</div>

					<!-- The WordPress Menu goes here -->
					<?php wp_nav_menu(
						array(
							'theme_location' 	=> 'primary',
							'depth'             => 2,
							'container'         => 'div',
							'container_class'   => 'collapse navbar-collapse',
							'menu_class' 		=> 'nav navbar-nav',
							'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
							'menu_id'			=> 'main-menu',
							'walker' 			=> new wp_bootstrap_navwalker()
						)
					); ?>

				</div><!-- .navbar -->
			</div>
			</nav><!-- .site-navigation -->
		</div>
	</div><!-- .container -->
</header><!-- #masthead -->

