<?php
/* * Template Name: Contact Page 
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 *
 * @package _tk
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="main-content pt70">

<?php 
			$thumbnail = '';
			if (function_exists('has_post_thumbnail')) {
			    if ( has_post_thumbnail() ) {
					 $thumbnail =  wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			    } else { 
			    	 $thumbnail = get_bloginfo( 'stylesheet_directory' )  . '/images/home-header.jpg';}
			} 
		?>
<div id="page-header" >
<div id="bg" style="background: url('<?php echo $thumbnail; ?>') no-repeat;">
</div>
<div id="cover">
<p>
<h1><?php the_title();?></h1>
</p>
</div>
</div>
<?php if( have_rows('contact_sections') ):  while ( have_rows('contact_sections') ) : the_row(); if( get_row_layout() == 'top_content' ):?>
	<div class="skewed-bg yellow">
	<div class="content padding" id="contact-form">
		<div id="spBgLogo"></div>
		<div class="contact-text col-xs-12 col-sm-6 col-md-6">
		<h3><?php the_sub_field('title');?></h3>
		<?php the_sub_field('text');?>
		</div>
		<div class="contact-form col-xs-12 col-sm-5 col-md-5">
		<h3><?php the_sub_field('form_title');?></h3>
			<?php the_sub_field('contact_form');?>
		</div>
	</div>
</div>
<?php endif; endwhile; endif;	?>
<?php if( have_rows('contact_sections') ):  while ( have_rows('contact_sections') ) : the_row(); if( get_row_layout() == 'about_phil' ):?>
	<div class="img-block contactPage-phil" style="background-image:url('/wp-content/uploads/2016/05/SP815_LIFESTYLE_male-phil-campbell-detail_lo-angle-front.png')">
		<div id="phil-bg-icon"></div>
		<div class="col-xs-10 col-sm-5 col-md-5 col-lg-4">
			<h3><?php the_sub_field('title');?></h3>
			<?php the_sub_field('text');?>
		</div>
	</div>
<?php endif; endwhile; endif;	?>



<?php if( have_rows('contact_sections') ):  while ( have_rows('contact_sections') ) : the_row(); if( get_row_layout() == 'lower_content' ):?>
	<div class="skewed-bg black">
		<div class="content contact-fill"></div>
	</div>
	<div id="contact-blackcap">
		
		<div class="fluid-container">
			<div class="row">
				<div class="col-md-offset-5 col-lg-offset-6 col-sm-12 col-md-5 col-lg-4">
				<h3><?php the_sub_field('title');?></h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-5 col-lg-6 contact-logo"> 
					<img src="/wp-content/uploads/2016/05/Matrix-logo_CMYK-white.png">
				</div>
				<div class="col-sm-12 col-md-5 col-lg-4 about_text">
					<?php the_sub_field('about_matrix');?>
				</div>	
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-5 col-lg-6 contact-logo">
					<img src="/wp-content/uploads/2016/05/Vision-Fitness-logo_2-color_metallic.png">
				</div>
				<div class="col-sm-12 col-md-5 col-lg-4 about_text">
					<?php the_sub_field('about_vision');?>
				</div>	
			</div>
			</div>
		</div>
	</div>
<?php endif; endwhile; endif;	?>

	</div><!-- close .container -->
</div><!-- close .main-content -->

			<?php the_content();?>

			<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
