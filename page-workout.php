<?php
/* * Template Name: Workout Page 
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 *
 * @package _tk
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="main-content pt70">

<?php 
			$thumbnail = '';
			if (function_exists('has_post_thumbnail')) {
			    if ( has_post_thumbnail() ) {
					 $thumbnail =  wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			    } else { 
			    	 $thumbnail = get_bloginfo( 'stylesheet_directory' )  . '/images/home-header.jpg';}
			} 
		?>
<div id="page-header" >
<div id="bg" style="background: url('<?php echo $thumbnail; ?>') no-repeat;">
</div>
<div id="cover">
<p>
<h1><?php the_title();?></h1>
</p>
</div>
</div>

<div class="skewed-bg black">
	<div class="container" id="workout" >
		<div class="top">
			<?php the_content();?>

		</div>
		<div id="athelete">
			<h3>WHAT TYPE</br>OF ATHLETE ARE YOU?</h3>
			
			<ul class="nav-pills" id="topTab">
				<?php if( have_rows('workout_tabs') ):  while ( have_rows('workout_tabs') ) : the_row(); if( get_row_layout() == 'beginner' ):?>
				<li class="load">
					<a href="javascript:void(0)" data-target="#top1" data-toggle="tab">
						<div class="drop-tab">
		  					<?php the_sub_field('tab_title');?>
		  					<div class="drop-tab-arrow-border"></div>
		  					<div class="drop-tab-arrow"></div>
						</div>
					</a>
					<p><?php the_sub_field('pre_text');?></p>
				</li>
				<?php endif; endwhile; endif; ?>
				<?php if( have_rows('workout_tabs') ):  while ( have_rows('workout_tabs') ) : the_row(); if( get_row_layout() == 'intermediate' ):?>
				<li class="load"><a href="javascript:void(0)" data-target="#top2" data-toggle="tab">
					<div class="drop-tab">
		  <?php the_sub_field('tab_title');?>
		  <div class="drop-tab-arrow-border"></div>
		  <div class="drop-tab-arrow"></div>
		</div></a><p><?php the_sub_field('pre_text');?></p></li>
				<?php endif; endwhile; endif; ?>
				<?php if( have_rows('workout_tabs') ):  while ( have_rows('workout_tabs') ) : the_row(); if( get_row_layout() == 'advanced' ):?>
				<li class="load"><a href="javascript:void(0)" data-target="#top3" data-toggle="tab"><div class="drop-tab">
		  <?php the_sub_field('tab_title');?>
		  <div class="drop-tab-arrow-border"></div>
		  <div class="drop-tab-arrow"></div>
		</div></a><p><?php the_sub_field('pre_text');?></p></li>
				<?php endif; endwhile; endif; ?>
				<?php if( have_rows('workout_tabs') ):  while ( have_rows('workout_tabs') ) : the_row(); if( get_row_layout() == 'Elite' ):?>
				<li class="load"><a href="javascript:void(0)" data-target="#top4" data-toggle="tab"><div class="drop-tab">
		  <?php the_sub_field('tab_title');?>
		  <div class="drop-tab-arrow-border"></div>
		  <div class="drop-tab-arrow"></div>
		</div></a><p><?php the_sub_field('pre_text');?></p></li>
		<?php endif; endwhile; endif; ?>
			</ul>

			<div class="tab-content">
				<!-- tab thing 1 -->
				<?php if( have_rows('workout_tabs') ):  while ( have_rows('workout_tabs') ) : the_row(); if( get_row_layout() == 'beginner' ):?>
				<div class="tab-pane" id="top1">
				 	<?php the_sub_field('description');?>
					<div class="subTab">
					<ul class="nav nav-pills" id="subTab1">
					  <li class="active"><a href="javascript:void(0)" data-target="#workout1-1" data-toggle="tab">Warm up</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout1-2" data-toggle="tab">Sprints</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout1-3" data-toggle="tab">Active Recovery Intervals</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout1-4" data-toggle="tab">Cool Down</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout1-5" data-toggle="tab">Post Workout</a></li>
					</ul>
					<div class="tab-content">
						<?php if( have_rows('warm_up' )):  while ( have_rows('warm_up') ) : the_row();?>
						<div class="tab-pane workout active" id="workout1-1">
					  		<div class="workout-outline">
								<h5> <?php the_sub_field('heading'); ?></h5>
						  		<div class="col-sm-12 col-md-6">
							  		<h6><?php the_sub_field('sub_heading'); ?></h6>
							  		<ul>
							  			<?php if( have_rows('list') ): ?>
						        		<?php while ( have_rows('list') ) : the_row();?>
						        		<li><?php the_sub_field('exercise'); ?></li>
						        		<?php endwhile; else :  // no rows found ?>
						        		<?php endif;?>		
							  		<ul>
						  		</div>
						  		<div class="col-sm-12 col-md-6">
						  			<?php the_sub_field('video'); ?>
						  		</div>
					 		</div>
					  	</div>
					 	<?php endwhile; endif; ?>
					 	<?php if( have_rows('Sprints' )):  while ( have_rows('Sprints') ) : the_row();?>
							<div class="tab-pane workout" id="workout1-2">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('active_recovery_intervals' )):  while ( have_rows('active_recovery_intervals') ) : the_row();?>
							<div class="tab-pane workout" id="workout1-3">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('cool_down' )):  while ( have_rows('cool_down') ) : the_row();?>
							<div class="tab-pane workout" id="workout1-4">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('post_workout' )):  while ( have_rows('post_workout') ) : the_row();?>
							<div class="tab-pane workout" id="workout1-5">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
					</div> 
					</div> 	
				  </div>
				  <?php endif; endwhile; endif;	?>
				 <!-- end tab 1-->
				 <!-- tab thing 2 -->
				<?php if( have_rows('workout_tabs') ):  while ( have_rows('workout_tabs') ) : the_row(); if( get_row_layout() == 'intermediate' ):?>
				<div class="tab-pane" id="top2">
				 	<?php the_sub_field('description');?>
					<div class="subTab">
					<ul class="nav nav-pills" id="subTab2">
					  <li class="active"><a data-target="#workout2-1" data-toggle="tab">Warm up</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout2-2" data-toggle="tab">Sprints</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout2-3" data-toggle="tab">Active Recovery Intervals</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout2-4" data-toggle="tab">Cool Down</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout2-5" data-toggle="tab">Post Workout</a></li>
					</ul>
					<div class="tab-content">
						<?php if( have_rows('warm_up' )):  while ( have_rows('warm_up') ) : the_row();?>
						<div class="tab-pane workout active" id="workout2-1">
					  		<div class="workout-outline">
								<h5> <?php the_sub_field('heading'); ?></h5>
						  		<div class="col-sm-12 col-md-6">
							  		<h6><?php the_sub_field('sub_heading'); ?></h6>
							  		<ul>
							  			<?php if( have_rows('list') ): ?>
						        		<?php while ( have_rows('list') ) : the_row();?>
						        		<li><?php the_sub_field('exercise'); ?></li>
						        		<?php endwhile; else :  // no rows found ?>
						        		<?php endif;?>		
							  		<ul>
						  		</div>
						  		<div class="col-sm-12 col-md-6">
						  			<?php the_sub_field('video'); ?>
						  		</div>
					 		</div>
					  	</div>
					 	<?php endwhile; endif; ?>
					 	<?php if( have_rows('Sprints' )):  while ( have_rows('Sprints') ) : the_row();?>
							<div class="tab-pane workout" id="workout2-2">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('active_recovery_intervals' )):  while ( have_rows('active_recovery_intervals') ) : the_row();?>
							<div class="tab-pane workout" id="workout2-3">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('cool_down' )):  while ( have_rows('cool_down') ) : the_row();?>
							<div class="tab-pane workout" id="workout2-4">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('post_workout' )):  while ( have_rows('post_workout') ) : the_row();?>
							<div class="tab-pane workout" id="workout2-5">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
					</div> 
					</div> 	
				  </div>
				  <?php endif; endwhile; endif;	?>
				 <!-- end tab 2-->
				 <!-- tab thing 3 -->
				<?php if( have_rows('workout_tabs') ):  while ( have_rows('workout_tabs') ) : the_row(); if( get_row_layout() == 'advanced' ):?>
				<div class="tab-pane" id="top3">
				 	<?php the_sub_field('description');?>
					<div class="subTab">
					<ul class="nav nav-pills" id="subTab2">
					  <li class="active"><a data-target="#workout3-1" data-toggle="tab">Warm up</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout3-2" data-toggle="tab">Sprints</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout3-3" data-toggle="tab">Active Recovery Intervals</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout3-4" data-toggle="tab">Cool Down</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout3-5" data-toggle="tab">Post Workout</a></li>
					</ul>
					<div class="tab-content">
						<?php if( have_rows('warm_up' )):  while ( have_rows('warm_up') ) : the_row();?>
						<div class="tab-pane workout active" id="workout3-1">
					  		<div class="workout-outline">
								<h5> <?php the_sub_field('heading'); ?></h5>
						  		<div class="col-sm-12 col-md-6">
							  		<h6><?php the_sub_field('sub_heading'); ?></h6>
							  		<ul>
							  			<?php if( have_rows('list') ): ?>
						        		<?php while ( have_rows('list') ) : the_row();?>
						        		<li><?php the_sub_field('exercise'); ?></li>
						        		<?php endwhile; else :  // no rows found ?>
						        		<?php endif;?>		
							  		<ul>
						  		</div>
						  		<div class="col-sm-12 col-md-6">
						  			<?php the_sub_field('video'); ?>
						  		</div>
					 		</div>
					  	</div>
					 	<?php endwhile; endif; ?>
					 	<?php if( have_rows('Sprints' )):  while ( have_rows('Sprints') ) : the_row();?>
							<div class="tab-pane workout" id="workout3-2">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('active_recovery_intervals' )):  while ( have_rows('active_recovery_intervals') ) : the_row();?>
							<div class="tab-pane workout" id="workout3-3">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('cool_down' )):  while ( have_rows('cool_down') ) : the_row();?>
							<div class="tab-pane workout" id="workout3-4">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('post_workout' )):  while ( have_rows('post_workout') ) : the_row();?>
							<div class="tab-pane workout" id="workout3-5">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
					</div> 
					</div> 	
				  </div>
				  <?php endif; endwhile; endif;	?>
				 <!-- end tab 3-->
				 <!-- tab thing 2 -->
				<?php if( have_rows('workout_tabs') ):  while ( have_rows('workout_tabs') ) : the_row(); if( get_row_layout() == 'Elite' ):?>
				<div class="tab-pane" id="top4">
				 	<?php the_sub_field('description');?>
					<div class="subTab">
					<ul class="nav nav-pills" id="subTab4">
					  <li class="active"><a data-target="#workout4-1" data-toggle="tab">Warm up</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout4-2" data-toggle="tab">Sprints</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout4-3" data-toggle="tab">Active Recovery Intervals</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout4-4" data-toggle="tab">Cool Down</a></li>
					  <li><a href="javascript:void(0)" data-target="#workout4-5" data-toggle="tab">Post Workout</a></li>
					</ul>
					<div class="tab-content">
						<?php if( have_rows('warm_up' )):  while ( have_rows('warm_up') ) : the_row();?>
						<div class="tab-pane workout active" id="workout4-1">
					  		<div class="workout-outline">
								<h5> <?php the_sub_field('heading'); ?></h5>
						  		<div class="col-sm-12 col-md-6">
							  		<h6><?php the_sub_field('sub_heading'); ?></h6>
							  		<ul>
							  			<?php if( have_rows('list') ): ?>
						        		<?php while ( have_rows('list') ) : the_row();?>
						        		<li><?php the_sub_field('exercise'); ?></li>
						        		<?php endwhile; else :  // no rows found ?>
						        		<?php endif;?>		
							  		<ul>
						  		</div>
						  		<div class="col-sm-12 col-md-6">
						  			<?php the_sub_field('video'); ?>
						  		</div>
					 		</div>
					  	</div>
					 	<?php endwhile; endif; ?>
					 	<?php if( have_rows('Sprints' )):  while ( have_rows('Sprints') ) : the_row();?>
							<div class="tab-pane workout" id="workout4-2">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('active_recovery_intervals' )):  while ( have_rows('active_recovery_intervals') ) : the_row();?>
							<div class="tab-pane workout" id="workout4-3">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('cool_down' )):  while ( have_rows('cool_down') ) : the_row();?>
							<div class="tab-pane workout" id="workout4-4">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
						 	<?php if( have_rows('post_workout' )):  while ( have_rows('post_workout') ) : the_row();?>
							<div class="tab-pane workout" id="workout4-5">
						  		<div class="workout-outline">
									<h5> <?php the_sub_field('heading'); ?></h5>
							  		<div class="col-sm-12 col-md-6">
								  		<h6><?php the_sub_field('sub_heading'); ?></h6>
								  		<ul>
								  			<?php if( have_rows('list') ): ?>
							        		<?php while ( have_rows('list') ) : the_row();?>
							        		<li><?php the_sub_field('exercise'); ?></li>
							        		<?php endwhile; else :  // no rows found ?>
							        		<?php endif;?>		
								  		<ul>
							  		</div>
							  		<div class="col-sm-12 col-md-6">
							  			<?php the_sub_field('video'); ?>
							  		</div>
						 		</div>
						  	</div>
						 	<?php endwhile; endif; ?>
					</div> 
					</div> 	
				  </div>
				  <?php endif; endwhile; endif;	?>
				 <!-- end tab 4-->
				

			</div>

		</div> 
	</div>
</div>

<div id="resource-blackcap">
	<div class="cap-inner-bg "></div>
</div>


	</div><!-- close .container -->
</div><!-- close .main-content -->

			

			<?php endwhile; // end of the loop. ?>
<script type="text/javascript">
jQuery( document ).ready( function( $ ) {
	$("ul#topTab a").click(function (e) {
	  e.preventDefault();  
	    $(this).tab('show');
	    $("#topTab > li.load").removeClass("load");
	    $("#topTab > li > p").hide();
	    //console.log("nop-worked");
	    //responsiveVideos.resize();
		setTimeout(function() {
			// $(".lazy-load-youtube").css("width", "400px");
			// $(".lazy-load-youtube").css("height", "200px");
			$(window).trigger('resize');
			console.log("window", window)
		}, 500);
	});

	$(".subTab ul li a").click(function (e) {
	  e.preventDefault();  
		setTimeout(function() {
			// $(".lazy-load-youtube").css("width", "400px");
			// $(".lazy-load-youtube").css("height", "200px");
			$(window).trigger('resize');
			console.log("window", window)
		}, 500);
	});

} );
</script>

<?php get_footer(); ?>
