<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */
?>
			

<footer id="colophon" class="site-footer" role="contentinfo">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row">
			<div class="site-footer-inner col-sm-12">

				<!-- <img src="/wp-content/uploads/2016/05/footer-logos.png"> -->
				<a href="http://world.visionfitness.com/" target="_blank" class="logo-link"><img src="/wp-content/uploads/2016/06/vision.png"></a>
				<a href="http://world.matrixfitness.com/en/" target="_blank" class="logo-link"><img src="/wp-content/uploads/2016/06/matrix.png"></a>
				<?php wp_nav_menu(
						array(
							'theme_location' 	=> 'secondary',
							'depth'             => 2,
							'container'         => 'div',
							'container_class'   => 'footer-nav',
							'menu_class' 		=> 'nav',
							'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
							'menu_id'			=> 'footer-menu',
							'walker' 			=> new wp_bootstrap_navwalker()
						)
					); ?>

				<div id="copyright">© 2016 Johnson Health Tech</div>
			</div>
		</div>
	</div><!-- close .container -->
</footer><!-- close #colophon -->

<?php wp_footer(); ?>

</body>
</html>
